<?php

include_once"includes/head.inc.php";
include_once"includes/menu.inc.php";
include_once"includes/categorias.inc.php";
include_once"includes/busqueda.inc.php";

$id = $_GET['id'];
if(!isset($id))
{
    header('Location: index.php');
}
else
{
    require_once "clases/anuncio.php";
    $datos = new anuncio();
    $elemento = $datos->obtener_por_id($id);
    if ($elemento)
    {
        include_once"vistas/anuncio.php";
    }
    else
    {
        echo 'no existe esa id';
    }
}

include_once"includes/footer.inc.php";






