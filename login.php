<?php

if (!isset($_POST['submit']))
{
    include_once"includes/head.inc.php";
    include_once"includes/menu.inc.php";
    include_once"includes/categorias.inc.php";
    include_once"includes/busqueda.inc.php";
    include_once"includes/slider.inc.php";

        require_once"vistas/login.php" ;
        generaFormulario($mensaje="");

    include_once"includes/footer.inc.php";
}
else
{
    $nick=$_POST['nick'];
    $password=$_POST['password'];

    require_once "clases/usuario.php";
    $elementos = new Usuario();

    $compruebaLoginOK=false;
    foreach ($elementos as $elemento)
    {
        $usuarios = new Usuario();
        $compruebaLoginOK = $usuarios->compruebaLogin($nick, $password);
    }
    if ($compruebaLoginOK)
    {
        header("Location: index.php");
    }
    else
    {

        include_once "includes/head.inc.php";
        include_once "includes/menu.inc.php";
        include_once "includes/categorias.inc.php";
        include_once "includes/busqueda.inc.php";
        include_once "includes/slider.inc.php";

        $campos = new Usuario();
        $mensaje = $campos->validaFormularioLogin($nick, $password);

        if ($mensaje)
        {
            require_once "vistas/error.php";
            require_once "vistas/login.php";
            generaFormulario($nick);
        }
        else
        {
            $mensaje .= "Contraseña o usuario incorrecto!!";
            require_once "vistas/error.php";
            require_once "vistas/login.php";
            generaFormulario();
        }
    }
    include_once "includes/footer.inc.php";
}

