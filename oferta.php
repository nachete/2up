<?php

require_once"clases/oferta.php";

$id = $_GET['id'];
require_once "clases/anuncio.php";
$datos = new anuncio();
$elemento = $datos->obtener_por_id($id);

if(!isset($_POST['submit']))
{
    include_once"includes/head.inc.php";
    include_once"includes/menu.inc.php";
    include_once"includes/categorias.inc.php";
    include_once"includes/busqueda.inc.php";

    require_once"vistas/oferta.php";
    generaFormulario($nick = "", $titulo = "", $texto = "", $importe = "");
}
else  // al pulsar el botón
{
    $nick=$_POST['nick'];
    $titulo=$_POST['titulo'];
    $texto=$_POST['texto'];
    $importe=$_POST['importe'];

    $datos = new Oferta();
    $mensaje = $datos->validaFormulario($nick,$titulo,$texto,$importe);

    if (empty($mensaje))
    {
        header("Location: anuncio.php?id=$id");
    }
    else
    {
        include_once"includes/head.inc.php";
        include_once"includes/menu.inc.php";
        include_once"includes/categorias.inc.php";
        include_once"includes/busqueda.inc.php";

        require_once "vistas/error.php";
        require_once "vistas/oferta.php";
        generaFormulario($nick,$titulo,$texto,$importe);
    }
}

include_once"includes/footer.inc.php";






