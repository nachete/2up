<?php

require_once "clases/anuncio.php";

include"includes/head.inc.php";
include"includes/menu.inc.php";
include"includes/categorias.inc.php";
include"includes/busqueda.inc.php";
include"includes/slider.inc.php";

if(isset($_GET['categoria']))
{
    $categoria = $_GET['categoria'];
    $filtrados_por_categoria = $datos->busca_por_categoria($categoria);
    $elementos =$filtrados_por_categoria;
    if(empty($elementos))
    {
        $mensaje = "Error en la categoria";
        include "vistas/error.php";
    }
    else
    {
        foreach ($elementos as $elemento)
        {
            include "vistas/productos.php";
        }
    }
}
else
{
    if(!isset($_POST['buscar']) || isset($_POST['restablecer']))
    {
        $datos = new anuncio();
        $elementos = $datos->obtener_todos();

        foreach ($elementos as $elemento)
        {
            include"vistas/productos.php";
        }
    }
    else
    {
        $datos = new anuncio();

        $filtrados_por_texto = $datos->busca_por_texto($_POST['texto']);
        $filtrados_por_precio =$datos->busca_por_precio($_POST['preciominimo'],$_POST['preciomaximo']);
        $filtrados_por_fecha = $datos->busca_por_fecha($_POST['fecha']);

        $elementos = array_merge($filtrados_por_texto,$filtrados_por_precio, $filtrados_por_fecha);

        if(empty($elementos))
        {
            $mensaje = "No se han encontrado coincidencias!!";
            include "vistas/error.php";
        }
        else
        {
            foreach ($elementos as $elemento)
            {
                include "vistas/productos.php";
            }
        }
    }
}

include"includes/footer.inc.php";


