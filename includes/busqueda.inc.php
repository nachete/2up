<?php

require_once "clases/anuncio.php";

?>

<div class="list-group">
    <form method="post">
        <fieldset>
            <legend>Texto a buscar:</legend>
            <input type="text" name="texto" placeholder="Introduzca el texto a buscar..." value="<?php if(isset($_POST['buscar']))  echo $_POST['texto'] ?>"/>
        </fieldset>
        <br>
        <fieldset>
            <legend>Rango de precio:</legend>

            <?php

            function generaRangoPrecio($preciominimo, $preciomaximo)
            {
                $anuncio = new anuncio();
            ?>

            <label>Precio mínimo:</label>
            <label id="min"><?php echo $preciominimo;?></label><label>€</label>
            <input type="range" id="rangevaluemin" name="preciominimo" step="5" min="<?php echo $anuncio->dame_precio_minimo();?>" max="<?php echo $anuncio->dame_precio_maximo();?>" value="<?php echo $preciominimo;?>" onchange="minprice()" />

            <p id="min"></p>
            <br>

            <label>Precio máximo:</label>
            <label id="max"><?php echo $preciomaximo;?></label><label>€</label>
            <input type="range" id="rangevaluemax" name="preciomaximo" step="5" min="<?php echo $anuncio->dame_precio_minimo();?>" max="<?php echo $anuncio->dame_precio_maximo();?>" value="<?php echo $preciomaximo;?>" onchange="maxprice()" />

            <?php

            }
            if(!isset($_POST['buscar']))
            {
                $anuncio = new anuncio();
                $preciominimo = $anuncio->dame_precio_minimo();
                $preciomaximo = $anuncio->dame_precio_maximo();
                generaRangoPrecio($preciominimo, $preciomaximo);
            }
            else
            {
                generaRangoPrecio($_POST['preciominimo'],$_POST['preciomaximo']);
            }

            ?>

        </fieldset>
        <br>
        <fieldset>
            <legend>Antigüedad:</legend>
            <select class="" name="fecha" >
                <option <?php if($_POST['fecha'] == 'hoy') echo"selected"; ?> value="hoy">Hoy</option>
                <option <?php if($_POST['fecha'] == 'ultima_semana') echo"selected"; ?> value="ultima_semana">Últimos 7 días</option>
                <option <?php if($_POST['fecha'] == 'ultimo_mes') echo"selected"; ?> value="ultimo_mes">Últimos 30 días</option>
            </select>
        </fieldset>
        <input type="submit" name="restablecer" value="X" class="btn btn-danger pull-right" title="Restablecer Filtros"/>
        <input type="submit" name="buscar" value="Buscar" class="btn btn-success pull-right" title="Buscar"/>
    </form>
</div>
</div>
<div class="col-md-9">