        </div>
    </div>
</div>

<div class="container">

    <hr>

    <!-- Footer -->
    <footer>
        <div class="row">
            <div class="col-lg-12">
                <p class="text-center">Copyright &copy; Ignacio Cuenca Moya · 2up 2015</p>
            </div>
        </div>
    </footer>

</div>
<!-- /.container -->

<!-- Custom js -->
<script src="js/custom.js"></script>

<!-- jQuery -->
<script src="js/jquery.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="js/bootstrap.min.js"></script>

<script>
    function minprice()
    {
        document.getElementById("min").innerHTML = document.getElementById("rangevaluemin").value;
    }
    function maxprice()
    {
        document.getElementById("max").innerHTML = document.getElementById("rangevaluemax").value;
    }

</script>

</body>
</html>