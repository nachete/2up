<div class="col-sm-4 col-lg-4 col-md-4">
    <a href="anuncio.php?id=<?php echo $elemento['idAnuncio']; ?>">
        <div class="thumbnail">
            <img src="<?php echo $elemento['imagen']; ?>" alt="producto1">
            <div class="caption">

                <h5><?php echo $elemento['titulo'];?>
                </h5><br>
                <h5 class="pull-right"><?php echo $elemento['precio'];?>€</h5>
                <h6> <span class="glyphicon glyphicon-calendar"></span><?php echo $elemento['fechaHora'];?>
                    <span class="glyphicon glyphicon-user"></span><?php echo $elemento['propietario'];?></h6>
                <p style="font-size:0.8em;"><?php echo $elemento['texto'];?></p>
            </div>
            <div class="ofertas">
                <p class="pull-right">Nº de Ofertas: <a href="ofertas.php"><?php echo $elemento['ofertas'];?></a></p>
                <p>
                    <?php

                    for($i = 0; $i<$elemento['ofertas']; $i++)
                    {
                        ?>
                            <span class="glyphicon glyphicon-send"></span>
                        <?php
                    }

                    ?>
                </p>
            </div>
        </div>
    </a>
</div>