<div class="row">
    <div class="col-sm-12 col-lg-12 col-md-12">

        <div class="thumbnail">
            <div class="wrapper">

                <div class="full-image"></div>

                <div class="thumbnails">

                    <?php

                    foreach ($elemento['imagenes'] as $imagen)
                    {
                        ?>
                        <label>
                        <input type="radio" name="full-image" checked>
                        <div class="full-image">
                            <img src="<?php echo $imagen; ?>" alt="Pic1" />
                        </div>
                        <img src="<?php echo $imagen; ?>"/>
                        </label>
                    <?php
                    }
                    ?>


                </div>

            </div>
            <!--<img class="img-responsive" src="http://placehold.it/800x320" alt=""> -->
            <div class="caption-full">
                <h4 class="pull-right"><?php echo $elemento['precio'];?>€</h4>
                <h4><a href="#" ><?php echo $elemento['titulo'];?></a>
                </h4>
                <h5> <span class="glyphicon glyphicon-calendar"></span> <?php echo $elemento['fechaHora'];?>
                    <span class="glyphicon glyphicon-user"></span><?php echo $elemento['propietario'];?></h5>
                <p><?php echo $elemento['texto'];?></p>
            </div>
            <div class="ofertas">
                <p class="pull-right">Nº de Ofertas: <a href="ofertas.php"><?php echo $elemento['ofertas'];?></a></p>
                <p>
                    <?php

                    for($i = 0; $i<$elemento['ofertas']; $i++)
                    {
                        ?>
                        <span class="glyphicon glyphicon-send"></span>
                        <?php
                    }

                    ?>
                </p>
            </div>
        </div>
        <div class="text-right">
            <a href="ofertas.php?id=<?php echo $id; ?>" class="btn btn-success">Ver Ofertas</a>
            <a href="oferta.php?id=<?php echo $id; ?>" class="btn btn-warning">Haz una oferta</a>
            <a href="index.php" class="btn btn-danger">¡Cómpralo!</a>
        </div>
    </div>
</div>

