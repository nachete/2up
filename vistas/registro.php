<?php

function generaFormulario($nombre,$nick,$telefono,$email)
{
    ?>

    <div class="row">
        <div class="col-sm-8 col-lg-8 col-md-8 col-sm-offset-2">
            <div class="account-wall">
                <h1 class="text-center login-title">Formulario de Registro</h1>
                <form action="<?php $_SERVER['PHP_SELF']?>"method="post" class="form-signin">
                    <input type="text" name="nombre" class="form-control" placeholder="Nombre Real" value="<?php echo $nombre; ?>" />
                    <input type="text" name="nick" class="form-control" placeholder="Nick" value="<?php echo $nick; ?>" />
                    <input type="text" name="telefono" class="form-control" placeholder="Teléfono" value="<?php echo $telefono; ?>">
                    <input type="text" name="email" class="form-control" placeholder="Email" value="<?php echo $email; ?>" />
                    <input type="password" name="password1" class="form-control" placeholder="Contraseña" />
                    <input type="password" name="password2" class="form-control" placeholder="Repita la contraseña" />
                    <input class="btn btn-lg btn-success btn-block" name="submit" type="submit" value="Enviar Datos" />
                </form>
            </div>
        </div>
    </div>

    <?php
}
