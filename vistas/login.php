<?php

function generaFormulario($nick="")
{

    ?>
        <div class="row">
            <div class="col-sm-6 col-md-4 col-md-offset-4">
                <div class="account-wall">
                    <!-- <img class="profile-img" src="imgs/warning.png" alt="Loguéate"> -->
                    <h1 class="text-center login-title">Formulario de Login</h1>
                    <form action="<?php $_SERVER['PHP_SELF']?>"method="post" class="form-signin">
                        <input type="text" name="nick" class="form-control" placeholder="Usuario" value="<?php echo $nick; ?>">
                        <input type="password" name="password" class="form-control" placeholder="Contraseña" >
                        <input class="btn btn-lg btn-success btn-block" name="submit" type="submit" value="Loguéate">
                        <label class="checkbox pull-left">
                            <input type="checkbox" value="remember-me">
                            Recuérdame
                        </label>
                        <a href="#" class="pull-right need-help">Necesitas ayuda? </a><span class="clearfix"></span>
                    </form>
                </div>
                <a href="#" class="text-center new-account">Crea una cuenta!</a>
            </div>
        </div>

    <?php

}

    ?>


