<div class="row">
    <div class="col-sm-12 col-lg-12 col-md-12">
        <div class="page-header">
            <h3>Se va a añadir una oferta a:
                <a href="anuncio.php?id=<?php echo $id; ?>" class="pull-right">
                    <?php echo $elemento['titulo']; ?>
                </a>
            </h3>
        </div>

        <?php

        function generaFormulario($nick,$titulo,$texto,$importe)
        {
            ?>
            <div class="row">
                <div class="col-sm-8 col-lg-8 col-md-8 col-sm-offset-2">
                    <div class="account-wall">
                        <h1 class="text-center login-title">Formulario de Oferta</h1>
                        <form action="<?php $_SERVER['PHP_SELF']?>"method="post" class="form-signin">
                            <input type="text" name="nick" class="form-control" placeholder="Usuario" value="<?php echo $nick; ?>" />
                            <input type="text" name="titulo" class="form-control" placeholder="Título" value="<?php echo $titulo; ?>" />
                            <input type="text" name="texto" class="form-control" placeholder="Texto" value="<?php echo $texto; ?>">
                            <input type="text" name="importe" class="form-control" placeholder="Importe" value="<?php echo $importe; ?>" />
                            <input class="btn btn-lg btn-success btn-block" name="submit" type="submit" value="Validar Oferta" />
                            <input type="hidden" name="id" value="<?php $_GET['id'];?>" />
                        </form>
                    </div>
                </div>
            </div>

            <?php
        }

        ?>
    </div>
</div>