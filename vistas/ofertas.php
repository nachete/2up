<?php

$id = $_GET['id'];
require_once "clases/oferta.php";
$oferta = new Oferta();
$ofertas = $oferta->obtener_todos();

require_once "clases/anuncio.php";
$articulo = new anuncio();
$anuncio = $articulo->obtener_por_id($id);

?>

<div class="col-sm-12 col-lg-12 col-md-12">

    <div class="page-header">
        <h3>Ofertas del artículo:
            <a href="anuncio.php?id=<?php echo $id; ?>" class="pull-right">
                <?php echo $anuncio['titulo']; ?>
            </a>
        </h3>
    </div>
    <!-- Table -->
    <table class="table">

        <?php

        echo "<th>Id Oferta</th><th>Propietario</th><th>Telefono</th><th>Título</th><th>Texto</th><th>Fecha y Hora</th><th>Importe</th>";
        foreach ($ofertas as $oferta)
        {
            if($oferta['idAnuncio']===$id)
            {
                require_once "clases/usuario.php";
                $user = new Usuario();
                $usuario = $user->obtener_propietario_oferta($oferta['propietario']);

        ?>
            <tr>
                <td><?php echo $oferta['idOferta']?></td>
                <td><?php echo $oferta['propietario'] ?></td>
                <td><?php echo $usuario['telefono'] ?></td>
                <td><?php echo $oferta['titulo'] ?></td>
                <td><?php echo $oferta['texto']; ?></td>
                <td><?php echo $oferta['fechaHora'] ?></td>
                <td><?php echo $oferta['importe'] ?></td>
            </tr>



        <?php
            }
        }
        ?>

    </table>
</div>


