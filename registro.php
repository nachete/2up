<?php

require_once"clases/usuario.php";

if(!isset($_POST['submit']))
{
    include_once"includes/head.inc.php";
    include_once"includes/menu.inc.php";
    include_once"includes/categorias.inc.php";
    include_once"includes/busqueda.inc.php";
    include"includes/slider.inc.php";

    require_once"vistas/registro.php";
    generaFormulario($nombre = "", $nick = "", $telefono = "", $email = "");
}
else  // al pulsar el botón
{
    $nombre=$_POST['nombre'];
    $nick=$_POST['nick'];
    $telefono=$_POST['telefono'];
    $email=$_POST['email'];
    $password1=$_POST['password1'];
    $password2=$_POST['password2'];

    $datos = new Usuario();
    $mensaje = $datos->validaFormularioRegistro($nombre,$nick,$telefono, $email, $password1, $password2);

    if (empty($mensaje))
    {
        header("Location: index.php");
    }
    else
    {
        include_once"includes/head.inc.php";
        include_once"includes/menu.inc.php";
        include_once"includes/categorias.inc.php";
        include_once"includes/busqueda.inc.php";

        require_once "vistas/error.php";
        require_once "vistas/registro.php";
        generaFormulario($nombre,$nick,$telefono, $email, $password1="", $password2="");
    }
}

include_once"includes/footer.inc.php";






