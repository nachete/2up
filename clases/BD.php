<?php

class BD
{
    protected $datos = array(
        'anuncios' => array(
            '0' => array(
                "idAnuncio" => "0",
                "titulo" => "Recreativa Arcade Pacman",
                "texto" => "    Se vende maquina pacman arcade con pantalla de 19, 2 jugadores, 6 botones por jugador,
                            2 altavoces y 2 usb externos para añadir juegos o configurar la maquina como quieras.",
                "propietario" => "nacho",
                "fechaHora" => "20-09-2015 11:34:00",
                "categoria" => "arcades",
                "precio" => "950",
                "ofertas" => "1",
                "imagen" => "imgs/pac_man_1.jpg",
                "imagenes" => array(    "0"=>"imgs/pac_man_1.jpg",
                                        "1"=>"imgs/pac_man_1.jpg",
                                        "2"=>"imgs/pac_man_1.jpg",
                                        "3"=>"imgs/pac_man_1.jpg"),
                "vendido" => "0",
            ),
            '1'=> array(
                "idAnuncio" => "1",
                "titulo" => "Recreativa Arcade Mario Kart",
                "texto" => "    Se vende maquina Mario Kart arcade con pantalla de 19, 2 jugadores, 6 botones por jugador,
                            2 altavoces y 2 usb externos para añadir juegos o configurar la maquina como quieras.",
                "propietario" => "lola",
                "fechaHora" => "05-10-2015 19:20:00",
                "categoria" => "arcades",
                "precio" => "1520",
                "ofertas" => "1",
                "imagen" => "imgs/mario_kart_1.jpg",
                "imagenes" => array(    "0"=>"imgs/mario_kart_1.jpg",
                                        "1"=>"imgs/mario_kart_1.jpg",
                                        "2"=>"imgs/mario_kart_1.jpg",
                                        "3"=>"imgs/mario_kart_1.jpg"),
                "vendido" => "0",
            ),
            '2'=> array(
                "idAnuncio" => "2",
                "titulo" => "Recreativa Bartop Metal Slug",
                "texto" => "    Se vende maquina Metal Slug bartop con pantalla de 19, 2 jugadores, 6 botones por jugador,
                            2 altavoces y 2 usb externos para añadir juegos o configurar la maquina como quieras.",
                "propietario" => "kiko",
                "fechaHora" => "11-10-2015 01:50:00",
                "categoria" => "bartops",
                "precio" => "350",
                "ofertas" => "1",
                "imagen" => "imgs/metal_slug_1.jpg",
                "imagenes" => array(    "0"=>"imgs/metal_slug_1.jpg",
                                        "1"=>"imgs/metal_slug_1.jpg",
                                        "2"=>"imgs/metal_slug_1.jpg",
                                        "3"=>"imgs/metal_slug_1.jpg"),
                "vendido" => "0",
            ),
            '3'=> array(
                "idAnuncio" => "3",
                "titulo" => "Recreativa Bartop Street Fighter",
                "texto" => "    Se vende maquina Street Fighter bartop con pantalla de 19, 2 jugadores, 6 botones por jugador,
                            2 altavoces y 2 usb externos para añadir juegos o configurar la maquina como quieras.",
                "propietario" => "kiko",
                "fechaHora" => "11-10-2015 01:50:00",
                "categoria" => "bartops",
                "precio" => "375",
                "ofertas" => "1",
                "imagen" => "imgs/street_fighter_1.jpg",
                "imagenes" => array(    "0"=>"imgs/street_fighter_1.jpg",
                                        "1"=>"imgs/street_fighter_1.jpg",
                                        "2"=>"imgs/street_fighter_1.jpg",
                                        "3"=>"imgs/street_fighter_1.jpg"),
                "vendido" => "0",
            ),
            '4'=> array(
                "idAnuncio" => "4",
                "titulo" => "Nintendo Nes",
                "texto" => "    Se vende videoconsola Nintendo Nes con un mando y todos sus cables funcionando como nueva.",
                "propietario" => "lola",
                "fechaHora" => "13-07-2015 15:22:03",
                "categoria" => "consolas",
                "precio" => "100",
                "ofertas" => "1",
                "imagen" => "imgs/nintendo_nes_1.jpg",
                "imagenes" => array(    "0"=>"imgs/nintendo_nes_1.jpg",
                                        "1"=>"imgs/nintendo_nes_1.jpg",
                                        "2"=>"imgs/nintendo_nes_1.jpg",
                                        "3"=>"imgs/nintendo_nes_1.jpg"),
                "vendido" => "0",
            ),
            '5'=> array(
                "idAnuncio" => "5",
                "titulo" => "Sega Dreamcast",
                "texto" => " Se vende videoconsola Sega Dreamcast con un mando y todos sus cables funcionando como nueva.",
                "propietario" => "kiko",
                "fechaHora" => "21-6-2015 09:25:49",
                "categoria" => "consolas",
                "precio" => "85",
                "ofertas" => "1",
                "imagen" => "imgs/sega_dreamcast_1.jpg",
                "imagenes" => array(    "0"=>"imgs/sega_dreamcast_1.jpg",
                                        "1"=>"imgs/sega_dreamcast_1.jpg",
                                        "2"=>"imgs/sega_dreamcast_1.jpg",
                                        "3"=>"imgs/sega_dreamcast_1.jpg"),
                "vendido" => "0",
            )
        ),
        'usuarios' => array(
            '0' => array(
                "idUsuario" => "0",
                "nombre" => "Ignacio Cuenca Moya",
                "nick" => "nacho",
                "password" => "nacho",
                "email" => "ignaciocuencamoya@gmail.com",
                "telefono" => "660316308",
                "rol" => "admin",
            ),
            '1'=> array(
                "idUsuario" => "1",
                "nombre" => "Yaiza Sánchez Martínez",
                "nick" => "yaiza",
                "password" => "yaiza",
                "email" => "yaizasanchez2010@gmail.com",
                "telefono" => "676135606",
                "rol" => "user",
            ),
            '2' => array(
                "idUsuario" => "2",
                "nombre" => "Kiko Veneno",
                "nick" => "kiko",
                "password" => "kiko",
                "email" => "kikoveneno@gmail.com",
                "telefono" => "679485125",
                "rol" => "admin",
            ),
            '3'=> array(
                "idUsuario" => "3",
                "nombre" => "Lola Flores",
                "nick" => "lola",
                "password" => "lola",
                "email" => "lolaflores@gmail.com",
                "telefono" => "623659854",
                "rol" => "user",
            ),
            '4' => array(
                "idUsuario" => "2",
                "nombre" => "Jesús",
                "nick" => "jesus",
                "password" => "jesus",
                "email" => "jesus@gmail.com",
                "telefono" => "656897852",
                "rol" => "user",
            ),
        ),
        'ofertas' => array(
            '0' => array(
                "idOferta" => "0",
                "idAnuncio" => "0",
                "propietario" => "nacho",
                "titulo" => "Mi humilde oferta 1",
                "texto" => "Como estamos en crisis y la cosa está muy mal, esto es lo que te puedo ofrecer",
                "fechaHora" =>"2015/09/29 15:20:59",
                "importe" => "150",
            ),
            '1' => array(
                "idOferta" => "1",
                "idAnuncio" => "1",
                "propietario" => "yaiza",
                "titulo" => "Mi humilde oferta 2",
                "texto" => "Como estamos en crisis y la cosa está muy mal, esto es lo que te puedo ofrecer",
                "fechaHora" =>"2015/09/29 15:20:59",
                "importe" => "200",
            ),
            '2' => array(
                "idOferta" => "2",
                "idAnuncio" => "2",
                "propietario" => "jesus",
                "titulo" => "Mi humilde oferta 3",
                "texto" => "Como estamos en crisis y la cosa está muy mal, esto es lo que te puedo ofrecer",
                "fechaHora" =>"2015/09/29 15:20:59",
                "importe" => "250",
            ),
            '3' => array(
                "idOferta" => "3",
                "idAnuncio" => "3",
                "propietario" => "kiko",
                "titulo" => "Mi humilde oferta 4",
                "texto" => "Como estamos en crisis y la cosa está muy mal, esto es lo que te puedo ofrecer",
                "fechaHora" =>"2015/09/29 15:20:59",
                "importe" => "300",
            ),
            '4' => array(
                "idOferta" => "4",
                "idAnuncio" => "4",
                "propietario" => "lola",
                "titulo" => "Mi humilde oferta 5",
                "texto" => "Como estamos en crisis y la cosa está muy mal, esto es lo que te puedo ofrecer",
                "fechaHora" =>"2015/09/29 15:20:59",
                "importe" => "400",
            ),
            '5' => array(
                "idOferta" => "5",
                "idAnuncio" => "5",
                "propietario" => "yaiza",
                "titulo" => "Mi humilde oferta 6",
                "texto" => "Como estamos en crisis y la cosa está muy mal, esto es lo que te puedo ofrecer",
                "fechaHora" =>"2015/09/29 15:20:59",
                "importe" => "500",
            )
        )
    );

    protected $tabla;

    function __construct($tabla)
    {
        $this->tabla = $tabla;
    }

    public function guardar($datos)
    {
        $this->conectar();
        array_push($this->datos[$this->tabla], $datos);
        $this->desconectar();
        return true;
    }

    public function obtener_todos()
    {
        return $this->datos[$this->tabla];
    }

    public function obtener_por_id($id)
    {
        return $this->datos[$this->tabla][$id];
    }

    public function eliminar_por_id($id)
    {
        unset($this->datos[$this->tabla][$id]);
        return true;
    }

    public function conectar()
    {

    }
    public function desconectar()
    {

    }
}