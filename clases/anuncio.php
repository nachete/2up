<?php

require_once 'BD.php';

class anuncio extends BD
{
    public $idAnuncio;
    public $titulo;
    public $texto;
    public $propietario;
    public $fechaHora;
    public $categoria;
    public $precio;
    public $ofertas;
    public $imagen;
    public $imagenes;
    public $vendido;

    function __construct()
    {
       parent::__construct('anuncios');
    }

    function busca_por_categoria($categoria)
    {
        $elementos = $this->obtener_todos();
        $resultado = array();
        for ($i = 0; $i<count($elementos); $i++)
        {
            if ($elementos[$i]['categoria']===$categoria)
            {
                $resultado[] =$elementos[$i];
            }
        }
        return $resultado;
    }

    function dameCategorias()
    {
        $elementos = $this->obtener_todos();
        $resultado = array();
        for ($i = 0; $i<count($elementos); $i++) {
            if ($elementos[$i]['categoria']!==$this->categoria)
            {
                $resultado[] =$elementos[$i]['categoria'];
            }
        }
        $resultado = array_unique($resultado);
        return $resultado;
    }

    /**
     * @param $fecha
     * @return array
     */
    function busca_por_fecha($fecha)
    {
        $elementos = $this->obtener_todos();
        $resultado = array();
        switch($fecha)
        {
            case "hoy":
                $coincide = false;
                $fechaHoyMax = date('d-m-Y H:i:s',strtotime('now'));
                $fechaHoyMin = date('d-m-Y H:i:s',strtotime('-1 day', strtotime($fechaHoyMax)));

                foreach ($elementos as $elemento)
                {

                    $fechaElemento = date('d-m-Y H:i:s',strtotime($elemento['fechaHora']));

                    if ($fechaHoyMin<= $fechaElemento && $fechaElemento<=$fechaHoyMax)
                    {
                        $coincide = true;
                    }
                    if($coincide)
                    {
                        $resultado["id-".$elemento['idAnuncio']] = $elemento;
                    }
                }
                break;

            case "ultima_semana":

                $coincide = false;
                $fechaHoyMax = date('d-m-Y H:i:s',strtotime('now'));
                $fechaHoyMin = date('d-m-Y H:i:s',strtotime('-7 days', strtotime($fechaHoyMax)));

                foreach ($elementos as $elemento)
                {
                    $fechaElemento = date('d-m-Y H:i:s',strtotime($elemento['fechaHora']));

                    if ($fechaHoyMin<= $fechaElemento && $fechaElemento<=$fechaHoyMax)
                    {
                        $coincide = true;
                    }
                    if($coincide)
                    {
                        $resultado["id-".$elemento['idAnuncio']] = $elemento;
                    }
                }
                break;

            case "ultimo_mes":

                $coincide = false;
                $fechaHoyMax = date('d-m-Y H:i:s',strtotime('now'));
                $fechaHoyMin = date('d-m-Y H:i:s',strtotime('-4 weeks', strtotime($fechaHoyMax)));

                foreach ($elementos as $elemento)
                {
                    $fechaElemento = date('d-m-Y H:i:s',strtotime($elemento['fechaHora']));

                    if ($fechaHoyMin<= $fechaElemento && $fechaElemento<=$fechaHoyMax)
                    {
                        $coincide = true;
                    }
                    if($coincide)
                    {
                        $resultado["id-".$elemento['idAnuncio']] = $elemento;
                    }
                }
                break;
        }
        return $resultado;
    }

    function busca_por_texto($texto)
    {
        $elementos = $this->obtener_todos();
        $resultado = array();
        $palabras = explode(' ', $texto);
        foreach ($elementos as $elemento)
        {
            $index = $elemento['idAnuncio'];

            $coincide = false;
            foreach ($palabras as $palabra)
            {
                if (stristr($elemento['texto'], $palabra) !== false)
                {
                    $coincide = true;
                }
                if($texto === "")
                {
                    $coincide= true;
                }
            }
            if ($coincide)
            {
                $resultado["id-".$elemento['idAnuncio']] = $elemento;
            }
        }
        if(strlen($texto)===0)
        {
            $vacio = array();
            return $vacio;
        }
        return $resultado;
    }

    function busca_por_precio($preciominimo, $preciomaximo)
    {
        $elementos = $this->obtener_todos();
        $resultado = array();
        for ($i = 0; $i<count($elementos); $i++)
        {
            if($elementos[$i]['precio']>=$preciominimo && $elementos[$i]['precio']<=$preciomaximo)
            {
                $resultado["id-".$elementos[$i]['idAnuncio']] =$elementos[$i];
            }
        }
        return $resultado;
    }

    function dame_precio_minimo()
    {
        $elementos = $this->obtener_todos();
        $resultado = array();
        for ($i = 0; $i<count($elementos); $i++)
        {
            $resultado[] = $elementos[$i]['precio'];
        }
        $preciominimo = min($resultado);
        return $preciominimo;
    }

    function dame_precio_maximo()
    {
        $elementos = $this->obtener_todos();
        $resultado = array();
        for ($i = 0; $i<count($elementos); $i++)
        {
            $resultado[] = $elementos[$i]['precio'];
        }
        $preciomaximo = max($resultado);
        return $preciomaximo;
    }

}