<?php

require_once"BD.php";

class Usuario extends BD
{
    public $idUsuario;
    public $nombre;
    public $nick;
    public $password;
    public $email;
    public $telefono;
    public $rol;

    function __construct()
    {
        parent::__construct('usuarios');
    }

    public function obtener_propietario_oferta($nick)
    {
        $elemento="telefono";
        $datos = new BD('usuarios');
        $elementos = $datos->obtener_todos();
        foreach ($elementos as $elemento)
        {
            if ($elemento['nick'] === $nick)
            {
                parent::obtener_por_id($elemento['idUsuario']);
                return $elemento;
            }
        }
        return $elemento;
    }

    public function compruebaLogin($nick,$password)
    {
        $datos = new BD('usuarios');
        $elementos = $datos->obtener_todos();
        foreach ($elementos as $elemento)
        {
            if ($elemento['nick'] === $nick && $elemento['password'] === $password)
            {
                return true;
            }
        }
        return false;
    }

    public function existeUsuario($nick, $password)
    {
        $datos = new BD('usuarios');
        $elementos = $datos->obtener_todos();
        $mensaje ="";
        foreach ($elementos as $elemento)
        {
            if ($elemento['nick'] === $nick && $elemento['password'] !== $password)
            {
                $mensaje .="Contraseña incorrecta!";
            }
            else
            {
                if($elemento['nick']!== $nick && $elemento['nick'] === "")
                {
                    $mensaje .= "El usuario no existe en la base de datos";
                }
            }
        }
        return $mensaje;
    }

    function validaFormularioLogin($nick, $password)
    {
        $mensaje = "";

        if (empty($nick) && empty($password))
        {
            $mensaje .= "Los campos no pueden estar vacíos!";
        }
        else
        {
            if (empty($nick))
            {
                $mensaje .= "El campo usuario no puede estar vacío!";
            }
            if (empty($password))
            {
                $mensaje .= "El campo password no puede estar vacío!";
            }
        }
        return $mensaje;
    }

    function validaFormularioRegistro($nombre ,$nick, $telefono, $email, $password1, $password2)
    {
        $mensaje = "";

        if (empty($nombre) && empty($nick) && empty($telefono) && empty($email) && empty($password1) && empty($password2))
        {
            $mensaje .= 'Los campos no pueden estar vacíos!<br>';
        }
        if (empty($nombre))
        {
            $mensaje .= 'El campo nombre no puede estar vacío!<br>';
        }
        if (empty($nick))
        {
            $mensaje .= 'El campo nick no puede estar vacío!<br>';
        }
        if (empty($telefono))
        {
            $mensaje .= 'El campo telefono no puede estar vacío!<br>';
        }
        if (empty($email))
        {
            $mensaje .= 'El campo email no puede estar vacío!<br>';
        }
        if (empty($password1))
        {
            $mensaje .= 'El campo password 1 no puede estar vacío!<br>';
        }
        if (empty($password2))
        {
            $mensaje .= 'El campo password 2 no puede estar vacío!<br>';
        }
        if ($password1 !== $password2)
        {
            $mensaje .= 'Las contraseñas deben ser iguales!<br>';
        }
        return $mensaje;
    }
}