<?php

require_once 'BD.php';

class Oferta extends BD
{
    public $idOferta;
    public $idAnuncio;
    public $fechaHora;
    public $propietario;
    public $titulo;
    public $texto;
    public $importe;

    function __construct()
    {
        parent::__construct('ofertas');
    }

    function validaFormulario($nick, $titulo, $texto, $importe)
    {
        $mensaje = "";

        if (empty($nick) && empty($titulo) && empty($texto) && empty($importe))
        {
            $mensaje .= "Los campos no pueden estar vacíos!<br>";
        }
        if (empty($nick))
        {
            $mensaje .= "El campo nick no puede estar vacío<br>";
        }
        if (empty($titulo))
        {
            $mensaje .= "El campo titulo no puede estar vacío<br>";
        }
        if (empty($texto))
        {
            $mensaje .= "El campo texto no puede estar vacío<br>";
        }
        if (empty($importe))
        {
            $mensaje .= "El campo importe no puede estar vacío<br>";
        }
        return $mensaje;
    }

}